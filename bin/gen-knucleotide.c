#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<string.h>
#include<time.h>
#define A(x) ({typeof(x) _=(x);if(!x){fprintf(stderr,"%s:%d %s\n",__FILE__,__LINE__,#x);exit(1);};_;})
#define i(n,x) for(int i=0;i<n;i++){x;}
#define j(n,x) for(int j=0;j<n;j++){x;}
#define k(n,x) for(int k=0;k<n;k++){x;}
#define MAX(a,b) ((a)>(b)?(a):(b))
#define MIN(a,b) ((a)<(b)?(a):(b))
int usage(char*s){fprintf(stderr,"%s j\n eg %s 2\n",s,s);return exit(1),1;}
int getj(int argc,char**argv){int r=atoi(argv[argc-1]);return r?r:usage(*argv);}
double rand1(){return MIN(0.99999999,((double)random())/RAND_MAX);}
long roll(long x){return floor(rand1()*x);}
char*rolls(long n,char*x){char*r=malloc(n);int len=strlen(x);i(n,r[i]=x[roll(len)]);return r;}
double fudge(double x,long y){double a=x*y;return y+a-roll((long)(2*a));}
void seed(){struct timespec t;A(!clock_gettime(CLOCK_REALTIME,&t));srandom(t.tv_nsec);}

int main(int c,char**v)
{seed();long n=1e3;i(getj(c,v),n*=10);char*s;s=A(malloc(n));
 char*  subs[5]={"GGTATTTTAATTTATAGT","GGTATTTTAATT","GGTATT","GGTA","GGT"};
 double props[5]={1e-5               , 1e-4         , 1e-3   , 1e-2 , 2e-2};
 long ns[5];i(5,ns[i]=(long)fudge(0.1,MAX(10,props[i]*n)));
 long*ixs[5];i(5,ixs[i]=malloc(sizeof(long)*ns[i]));
 i(5,j(ns[i],ixs[i][j]=roll(n)));
 int lens[5];i(5,lens[i]=strlen(subs[i]));
 char*r=rolls(n,"GGGGAAATC");
 i(5,j(ns[i],k(lens[i],r[(ixs[i][j]+k)%n]=subs[i][k])))
 long lines=n/75;
 i(lines,j(75,putchar(r[i*75+j]))putchar('\n'))
 i(n%75,putchar(r[lines*75+i]))(n%75)&&putchar('\n');
 return 0;
}
